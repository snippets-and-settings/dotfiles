#           _              
#   _______| |__  _ __ ___ 
#  |_  / __| '_ \| '__/ __|
#   / /\__ \ | | | | | (__ 
#  /___|___/_| |_|_|  \___|
                        

# Aliases
alias dotfiles="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"
alias mirrorUpdate="sudo pacman-mirrors --fasttrack 5 && sudo pacman -Syyu"
alias zshconfig="nano ~/.zshrc"
alias ohmyzsh="nano ~/.oh-my-zsh"

# Path to your oh-my-zsh installation.
export ZSH="/home/eldrik/.oh-my-zsh"

# Theme
ZSH_THEME="powerlevel9k/powerlevel9k"

# Plugins
plugins=(
    git
    zsh-completions
    zsh-autosuggestions
    zsh-syntax-highlighting
)
autoload -U compinit && compinit

# Show OS info when opening a new terminal
neofetch

POWERLEVEL9K_TIME_FORMAT="%D{%H:%M}"

# Disable auto-correction
DISABLE_CORRECTION="true"
unsetopt correct
unsetopt correct_all

# Command execution time stamp shown in the history command output
HIST_STAMPS="mm/dd/yyyy"

source $ZSH/oh-my-zsh.sh
